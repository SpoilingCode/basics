package edu.opendev.guess;

/**
 * Created by ralex on 23.08.16.
 */
public interface Respondent {

    int nextAnswer();

}
