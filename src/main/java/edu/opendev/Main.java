package edu.opendev;

import edu.opendev.basics.ArrayDemo;
import edu.opendev.basics.WelcomeDemo;
import edu.opendev.basics.ControlFlowDemo;
import edu.opendev.basics.SwitchStatementDemo;
import edu.opendev.guess.GameGuess;
import edu.opendev.guess.Player;

import java.io.FileNotFoundException;

/**
 * типы данных
 * арифметические операции
 * операции отношения
 * битовые операции
 * классы Math и StrictMath
 * приведение типов
 */

public class Main {

    final static double pi = 3.14;//3.14D, 3.14F

    /**
     * Точка входа в приложение
     *
     * @param args параметры запуска
     */
    public static void main(String[] args) throws FileNotFoundException {
        /*GameGuess gg = new GameGuess(100, new Player());
        gg.start();*/

        /*GameGuess gg2 = new GameGuess(100, new Guesser(100));
        gg2.start();*/


        /*SwitchStatementDemo.stringCase("СенТяБрь");
        SwitchStatementDemo.breakCase(1);
        WelcomeDemo.welcomeEtc();
        ControlFlowDemo.whileStatement();*/

        ArrayDemo.dimArray();
        ArrayDemo.multiDimArray();
        ArrayDemo.triangularMatrix();
        ArrayDemo.copyArrayDemo();



    }

    // SpoilingCode


}

